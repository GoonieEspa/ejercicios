<?php

use cursophp7\app\exception\AppException;
use cursophp7\app\exception\QueryException;
use cursophp7\app\repository\CategoriaRepository;
use cursophp7\app\repository\ImagenGaleriaRepository;

$errores=[];
$descripcion = '';
$mensaje = '';

try {
    $imgRepository = new ImagenGaleriaRepository();
    $categoriaRepository = new CategoriaRepository();

    $imagenes = $imgRepository->findAll();
    $categorias = $categoriaRepository->findAll();

}
catch (QueryException $queryException)
{
    $errores[] = $queryException->getMessage();
}
catch (AppException $appException)
{
    $errores[] = $appException->getMessage();
}

require __DIR__ . '/../views/galeria.view.php';
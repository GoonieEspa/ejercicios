<?php


use cursophp7\app\repository\AsociadoRepository;
use cursophp7\app\repository\ImagenGaleriaRepository;
use cursophp7\app\utils\Utils;

$imgRepository = new ImagenGaleriaRepository();
$imagenes = $imgRepository->findAll();

$asociadoRepository = new AsociadoRepository();
$asociados = $asociadoRepository->findAll();


$asociados = Utils::obtenerArrayReducido($asociados,3);
require __DIR__ . '/../views/index.view.php';

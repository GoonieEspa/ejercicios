<?php

use cursophp7\app\exception\NotFoundExeption;
use cursophp7\core\Request;
use cursophp7\core\Router;

try
{
    require 'core/bootstrap.php';

    require Router::load('app/routes.php')->direct(implode([Request::uri()]),Request::method());
}
catch (NotFoundExeption $notFoundException)
{
    die($notFoundException->getMessage());
}
<?php
namespace cursophp7\core\database;



use cursophp7\app\exception\NotFoundExeption;
use cursophp7\app\exception\QueryException;
use cursophp7\core\App;
use PDO;
use PDOException;

abstract class QueryBuilder
{
    /**
     * @var PDO
     */
    private $connection;
    /**
     * @var string
     */
    private $table;
    /**
     * @var string
     */
    private $classEntity;

    public function __construct(string $table, string $classEntity)
    {
        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }

    /**
     * @param string $sql
     * @return array
     * @throws QueryException
     */
    private function executeQuery(string $sql) : array
    {
        $pdoStatements = $this->connection->prepare($sql);

        if ($pdoStatements->execute() === false)
            throw new QueryException("No se ha podido ejecutar la query solicitada");

        return $pdoStatements->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classEntity);
    }
    /**
     * @param string $table
     * @param string $classEntity
     * @return array
     * @throws QueryException
     */
    public function findAll() : array
    {
        $sql = "SELECT * FROM  $this->table";

        return $this->executeQuery($sql);
    }

    /**
     * @param int $id
     * @return IEntity
     * @throws NotFoundExeption
     * @throws QueryException
     */
    public function find(int $id) : IEntity
    {
        $sql = "SELECT * FROM  $this->table WHERE id=$id";

        $result = $this->executeQuery($sql);

        if (empty($result))
            throw new NotFoundExeption("No se ha encontrado ningún elemnto con id");

        return $result[0];
    }

    public function save(IEntity $entity) : void
    {
        try
        {
            $parameters = $entity->toArray();

            $sql = sprintf(
                'insert into %s (%s) values (%s)',
                $this->table,
                implode(', ', array_keys($parameters)),
                ':' . implode(', :', array_keys($parameters))
            );
            $statement = $this->connection->prepare($sql);

            $statement->execute($parameters);
        }
        catch (PDOException $exception)
        {
            throw new QueryException('Error al insertar en la base de datos');
        }
    }

    /**
     * @param array $parameters
     * @return string
     */
    public function getUpdates(array $parameters)
    {
        $updates = '';

        foreach ($parameters as $key => $value)
        {
            if ($key !== 'id')
            {
                if ($updates !== '')
                    $updates .= ', ';
                $updates .= $key . '=:' . $key;
            }
        }
        return $updates;
    }

    /**
     * @param IEntity $entity
     * @throws QueryException
     *
     */
    public function update(IEntity $entity):void
    {
        try
        {
            $parameters = $entity->toArray();

            $sql = sprintf(
                'UPDATE %s SET %s WHERE id=:id',
                $this->table,
                $this->getUpdates($parameters)
            );

            $statement = $this->connection->prepare($sql);

            $statement->execute($parameters);

        }
        catch (PDOException $pdoException)
        {
            throw new QueryException('Error al actualizar el elemento con id' . $parameters['id']);
        }

    }

    /**
     * @param callable $fnExecuteQuerys
     * @throws QueryException
     */
    public function executeTransaction(callable $fnExecuteQuerys)
    {
        try
        {
            $this->connection->beginTransaction();

            $fnExecuteQuerys();

            $this->connection->commit();
        }
        catch (PDOException $pdoException)
        {
            $this->connection->rollBack();

            throw new QueryException('No se ha podido realizar la operación');
        }
    }
}
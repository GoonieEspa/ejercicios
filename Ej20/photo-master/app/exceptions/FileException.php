<?php

namespace cursophp7\app\exception;

use Exception;

class FileException extends Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
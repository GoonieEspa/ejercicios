<?php

use cursophp7\app\entity\Asociado;
use cursophp7\app\exception\FileException;
use cursophp7\app\exception\QueryException;
use cursophp7\app\exception\ValidationException;
use cursophp7\app\repository\AsociadoRepository;
use cursophp7\app\utils\File;

$errores = [];

try{
    $asociadoRepository = new AsociadoRepository();

if ($_SERVER['REQUEST_METHOD']=== 'POST') {

        $nombre = trim(htmlspecialchars($_POST['nombre']));

        if (empty($nombre))
            throw new ValidationException('El nombre no puede quedar vacío');


        $descripcion = trim(htmlspecialchars($_POST['descripcion']));

        $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
        $imagenFile = new File('logo', $tiposAceptados);

        $imagenFile->saveUploadFile(Asociado::RUTA_IMAGENES_ASOCIADOS);

        $asociado = new Asociado($nombre, $imagenFile->getFileName(), $descripcion);

        $asociadoRepository->save($asociado);
        $mensaje = "Se ha guardado el asociado " . $asociado->getNombre();
        $descripcion = '';
        $nombre = '';

}
    $asociados = $asociadoRepository->findAll();
}
catch (FileException $fileException)
{
    $errores[] = $fileException->getMessage();
}
catch (ValidationException $validationException)
{
    $errores[] = $validationException->getMessage();
}
catch (QueryException $queryException)
{
    $errores[] = $queryException->getMessage();
}
require __DIR__ . '/../views/asociados.view.php';
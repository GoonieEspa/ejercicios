<?php

use cursophp7\app\entity\Mensaje;
use cursophp7\app\exception\QueryException;
use cursophp7\app\exception\ValidationException;
use cursophp7\app\repository\MensajeRepository;

$errores = [];
$mensaje = '';
$nombre = '';
$apellidos = '';
$email = '';
$asunto = '';
$texto = '';

try{
    $mensajeRepository = new MensajeRepository();

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $nombre = trim(htmlspecialchars($_POST['nombre']));
    $apellidos = trim(htmlspecialchars($_POST['apellidos']));
    $email = trim(htmlspecialchars($_POST['email']));
    $asunto = trim(htmlspecialchars($_POST['asunto']));
    $texto = trim(htmlspecialchars($_POST['texto']));

    if(empty($nombre))
        $errores[] = "El nombre no se puede quedar vacío";

    if(empty($email))
        $errores[] = "el e-mail no se puede quedar vacío";
    else
    {
        if(filter_var($email, FILTER_VALIDATE_EMAIL)===false)
            $errores[]="El e-mail no es válido";
    }

    if(empty($asunto))
        $errores[] = "El asunto no se puede quedar vacío";

    if(empty($errores))
    {
        $mensaje = new Mensaje($nombre, $apellidos, $asunto, $email, $texto);

        $mensajeRepository->save($mensaje);

        $mensaje = 'Hemos recibido su mensaje';

    }
}
}
catch (ValidationException $validationException)
{
    $errores[] = $validationException->getMessage();
}
catch (QueryException $queryException)
{
    $errores[] = $queryException->getMessage();
}

require __DIR__ . '/../views/contact.view.php';
<?php
namespace ProyectoWeb\app\controllers\admin;

use claviska\SimpleImage;
use ProyectoWeb\entity\Product;
use ProyectoWeb\utils\Forms\custom\MyFormControl;
use ProyectoWeb\utils\Forms\FileElement;
use ProyectoWeb\utils\Forms\FormElement;
use ProyectoWeb\utils\Forms\ImgElement;
use ProyectoWeb\utils\Forms\InputElement;
use ProyectoWeb\utils\Forms\LabelElement;
use ProyectoWeb\utils\Forms\NumberElement;
use ProyectoWeb\utils\Forms\TextareaElement;
use ProyectoWeb\utils\Validator\FileNotEmptyValidator;
use ProyectoWeb\utils\Validator\MimetypeValidator;
use ProyectoWeb\utils\Validator\NotEmptyValidator;
use Psr\Container\ContainerInterface;
use ProyectoWeb\core\App;

class ProductController
{
    protected $container;

    // constructor receives container instance
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
    public function home($request, $response, $args) {
        $pageheader = "Productos";
        
        return $this->container->renderer->render($response, "productos.view.php", compact('pageheader'));
        
   }

    public function getForm(Product $producto=null):FormElement
    {

        $nombre = new InputElement('text');

        $nombre

            ->setName('nombre')

            ->setId('nombre')

            ->setValidator(new NotEmptyValidator('El nombre es obligatorio', true));

        $nombre = new MyFormControl($nombre, 'Nombre', 'col-xs-12');

        $descripcion = new TextareaElement();

        $descripcion->setName("descripcion")->setId("descripcion")->setValidator(new NotEmptyValidator("La descripción es obligatória"));

        //$foto = new ImgElement();

        $precio = new NumberElement();
        $precio->setName("precio")->setId("precio")->setValidator(new NotEmptyValidator("El precio es obligatório"));

        $form = new FormElement($this->container->router->pathFor('new-product'));

        $form->setCssClass('form-horizontal');

        //$form->appendChild($nombre)->appendChild()->appendChild($descripcion)->appendChild($precio);
        $form->appendChild($nombre);

        return $form;


    }

    public function add($request, $response, $args) {

        $pageheader = "Productos: nueva";

        $form = $this->getForm();

        $formElements = $form->getFormElements();

        /*$foto = $formElements['foto'];

        $foto->saveUploadedFile(APP::get('rootDir') . Product::RUTA_IMAGENES); */

// Create a new SimpleImage object

        //$simpleImage = new \claviska\SimpleImage();

        $simpleImage = new SimpleImage();

        $simpleImage

            ->fromFile(APP::get('rootDir') . Product::RUTA_IMAGENES . $foto->getFileName())

            ->resize(600)

            ->toFile(APP::get('rootDir') . Product::RUTA_IMAGENES . '600_' . $foto->getFileName())

            ->resize(256)

            ->toFile(APP::get('rootDir') . Product::RUTA_IMAGENES . '256_' . $foto->getFileName());


//El carrusel puede estar vacío

$carrusel = $formElements['carrusel'];

if (!empty($carrusel->getFileName())) {

    $carrusel->saveUploadedFile(APP::get('rootDir') . Product::RUTA_IMAGENES_CARRUSEL);

    $simpleImage

        ->fromFile(APP::get('rootDir') . Product::RUTA_IMAGENES . $foto->getFileName())

        ->resize(800, 300)

        ->toFile(APP::get('rootDir') . Product::RUTA_IMAGENES_CARRUSEL . $foto->getFileName());

}


$producto = new Product(null, $formElements['nombre']->getValue(),

    $formElements['descripcion']->getValue(),

    $formElements['categoria']->getValue(),

    $formElements['precio']->getValue(),

    $formElements['foto']->getFileName(),

    ($formElements['destacado']->isChecked() ? 1: 0),

    $formElements['carrusel']->getFileName());

$repositorio=null;

$repositorio->save($producto);

    }



   
 


}

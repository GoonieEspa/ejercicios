
    </div>

</div>

</div>
<!-- /.container -->

<div class="container" id='footer'>
<!-- Footer -->
<footer>
<div class="row">
  <div class="col-lg-12">
      <p>Copyright &copy; Plantas El Caminàs <?=date("Y"); ?></p>
  </div>
</div>
</footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="<?=$basePath;?>/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?=$basePath;?>/js/bootstrap.min.js"></script>
    <script src="<?=$basePath;?>/js/plugins/morris/morris-data.js"></script>

    <script>

        function checkDelete(){

            //Siempre que una acción no se pueda deshacer hay que pedir confirmación al usuario

            if (confirm("¿Seguro que desea borrar este elemento?"))

                return true;

            else

                return false;

        }

    </script>
</body>

</html>
<?php

namespace ProyectoWeb\utils\Forms;

use ProyectoWeb\utils\Forms\Element;



class ImgElement extends Element

{

    /**

     * @var string

     */

    private $src;

    /**

     * @var string

     */

    private $alt;

    /**

     * @var string

     */

    private $title;

    private $fileName;

    public function __construct(string $src, string $alt = '', string $title = '')

    {

        $this->src = $src;

        $this->alt = $alt;

        $this->title = $title;

        $this->fileName = "";

        parent::__construct();

    }



    public function render(): string

    {

        $html = 

            "<img src='" . $this->src . "' alt='" .  $this->alt . "' title='" .  $this->title . "' " . 

            $this->renderAttributes() .

            ">";

        return $html;

    }

    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Guarda en $desPath el fichero $this->fileName
     * @param string $destPath
     * @throws FileException
     */
    public function saveUploadedFile(string $destPath)
    {
        $ruta =  $destPath . $this->fileName;

        if (true === is_file($ruta)){
            $idUnico = time();
            $this->fileName = $idUnico  . "_" . $this->fileName;
            $ruta =  $destPath . $this->fileName;
        }
        if (false === @move_uploaded_file($_FILES[$this->name]["tmp_name"], $ruta)){
            throw new FileException("No se puede mover el fichero a su destino");
        }
    }

}
<?php

namespace ProyectoWeb\utils\Forms;

use ProyectoWeb\utils\Forms\DataElement;
class InputElement extends DataElement
{
    /**
     * Tipo del input
     *
     * @var string
     */
    protected $type;


    public function __construct()

    {

        $this->attributes = [];

    }

    /**
     * Genera el HTML del elemento
     *
     * @return string
     */
    public function render(): string
    {
        $this->setPostValue();
        $html = "<input type='{$this->type}' name='{$this->name}'" ; 
        
        if ('POST' === $_SERVER['REQUEST_METHOD']) {           
            $html .= " value='" . $this->getValue() . "'";
        } else {
            $html .= " value='{$this->defaultValue}'";
        }
        $html .= $this->renderAttributes();
        $html .= '>';
        return $html;
    }

    public function setAttribute(string $key, string $value) {

        $this->attributes[$key] = $value;

    }

    protected function renderAttributes(): string

    {

        $html = (!empty($this->id) ? " id='$this->id' " : '');

        $html .= (!empty($this->cssClass) ? " class='$this->cssClass' " : '');

        $html .= (!empty($this->style) ? " style='$this->style' " : '');

        foreach($this->attributes as $key => $value) {

            $html .= " $key=\"$value\" ";

        }

        return $html;

    }

}

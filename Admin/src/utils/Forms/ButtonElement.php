<?php
namespace ProyectoWeb\utils\Forms;

use  ProyectoWeb\utils\Forms\DataElement;

class ButtonElement extends DataElement
{
    /**
     * Texto del botón
     *
     * @var string
     */
    private $text;
    private $atributos;
    
    public function __construct(string $text)
	{
        $this->text = $text;
        $this->atributos = [];
        parent::__construct();
    }

    public function render(): string
    {
       return 
            "<button type='submit' " .

            $this->getAtributos()

            .

                (!empty($this->name) ? " name='$this->name' " : '') .
                $this->renderAttributes() . 
            ">{$this->text}</button>";
       
    }

    public function getAtributos(){

        $result = "";

        for($k=0; $k<=count($this->atributos)-1;$k++){

            $result = $this->atributos[$k]["nombre"] . "='" . $this->atributos[$k]["valor"] . "' ";

            }

            return $result;

    }


    public function setAttribute(string $key, string $value) {

        $this->attributes[$key] = $value;

    }

    public function addAtributos($nombreAtributo, $valorAtributo)
    {
        array_push($this->atributos,  []);

        $this->atributos[count($this->atributos)-1]["nombre"] = $nombreAtributo;
        $this->atributos[count($this->atributos)-1]["valor"] = $valorAtributo;

    }
    
    /**

     * Devuelve true si éste ha sido el botón enviado con el formulario

     *

     * @return boolean

     */

    public function isSubmitted(): bool {

        if (empty($this->name)) {

            return false;

        }

        return (isset($_POST[$this->name]));

    }
}
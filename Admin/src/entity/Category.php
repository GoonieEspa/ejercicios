<?php

namespace ProyectoWeb\entity;
use ProyectoWeb\entity\Entity;

class Category extends Entity{

    private $id;
    private $nombre;
    private $icon;



    public function __construct(int $id = null, string $nombre = '', string $icon = ''){

        $this->id = $id;
       
        $this->nombre = $nombre;
       
        $this->icon = $icon;
       
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(string $icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;
    }


    public function toArray(): array
    {
        return [
            'id'=> $this->getId(),
            'nombre'=> $this->getNombre(),
            'icon'=>$this->getIcon()
        ];
    }
}





?>
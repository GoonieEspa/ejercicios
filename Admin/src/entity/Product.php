<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/02/19
 * Time: 16:42
 */

namespace ProyectoWeb\entity;
use ProyectoWeb\entity\Entity;

class Product extends Entity{

    const RUTA_IMAGENES = '/imgs/';

    private $id;
    private $nombre;
    private $descripcion;
    private $categoria;
    private $precio;
    private $foto;
    

    /**
     * Product constructor.
     * @param $id
     * @param $nombre
     * @param $descripcion
     * @param $categoria
     * @param $precio
     * @param $foto
     */
    public function __construct($id = null, $nombre = "", $descripcion = "", $categoria = 0, $precio = 0, $foto = "", $destacado = 0, $carrusel = '')
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->categoria = $categoria;
        $this->precio = $precio;
        $this->foto = $foto;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @return mixed
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param mixed $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }

    public function toArray(): array
    {
        // TODO: Implement toArray() method.
    }


}
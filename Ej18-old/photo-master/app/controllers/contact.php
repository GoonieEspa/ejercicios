<?php
require_once 'utils/utils.php';
require_once 'entity/Mensaje.php';
require_once 'repository/MensajeRepository.php';
require_once 'database/Connection.php';

$errores=[];
$mensaje='';
$nombre= '';
$apellido='';
$email='';
$tema='';
$texto='';

try {
    

    $mensajeRepository = new MensajeRepository();

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        $nombre = trim(htmlspecialchars($_POST['nombre']));
        $apellido = trim(htmlspecialchars($_POST['apellido']));
        $email = trim(htmlspecialchars($_POST['email']));
        $tema = trim(htmlspecialchars($_POST['tema']));
        $texto = trim(htmlspecialchars($_POST['mensaje']));


        if (empty($nombre)) {
            $errores[] = "El nombre no se puede quedar vacío";
        }
        if (empty($email)) {
            $errores[] = "El email no se puede quedar vacío";
        } else {
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                $errores[] = "El email no es válido";
            }
        }
        if (empty($tema)) {
            $errores[] = "El asunto no se puede quedar vacío";
        }
        if (empty($errores)) {

            $mensaje=new Mensaje($nombre, $apellido,$tema,$email,$texto);

            $mensajeRepository->save($mensaje);



            $mensaje = "Hemos recibido su mensaje";

        }
    }


}catch(ValidationException $validationException){
    $errores[]=$validationException->getMessage();
}
catch(QueryException $queryException){
    $errores[]=$queryException->getMessage();
}

require __DIR__ . '/../views/contact.view.php';
?>
<?php
require_once 'utils/utils.php';
require_once 'exception/FileException.php';
require_once 'exception/AppException.php';
require_once 'exception/QueryException.php';
require_once 'utils/File.php';
require_once 'entity/ImageGallery.php';
require_once 'entity/Categoria.php';
require_once 'repository/ImagenGaleriaRepository.php';
require_once 'repository/CategoriaRepository.php';
require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';
require_once 'core/App.php';

$errores=[];
$descripcion='';
$mensaje='';
$imagenes=[];

try{

    
    $imgRepository=new ImagenGaleriaRepository();
    $categoriaRepository=new CategoriaRepository();

    if($_SERVER['REQUEST_METHOD']==='POST'){


            $descripcion=trim(htmlspecialchars($_POST['descripcion']));
            $categoria=trim(htmlspecialchars($_POST['categoria']));

            if(empty($categoria)){
                throw new ValidationException('No se ha recibido la categoría');
            }
            $tiposAceptados=['image/jpeg','image/png','image/gif'];
            $imagen=new File('imagen',$tiposAceptados);

            $imagen->saveUploadFile(ImageGallery::RUTA_IMAGENES_GALLERY);
            $imagen->copyFile(ImageGallery::RUTA_IMAGENES_GALLERY,ImageGallery::RUTA_IMAGENES_PORTFOLIO);



            $imagenGaleria = new ImageGallery($imagen->getFileName(),$descripcion,$categoria);

            $imgRepository->guarda($imagenGaleria);

                $descripcion='';
                $mensaje='Se ha guardado la imagen';



    }

    $imagenes=$imgRepository->findAll();
    $categorias=$categoriaRepository->findAll();

}
catch(FileException $fileException){

    $errores[]=$fileException->getMessage();
}
catch(QueryException $queryException){
    $errores[]=$queryException->getMessage();
}
catch(AppException $appException){
    $errores[]=$appException->getMessage();
}
catch(ValidationException $validationException){
    $errores[]=$validationException->getMessage();
}
require __DIR__ . '/../views/galeria.view.php';



?>
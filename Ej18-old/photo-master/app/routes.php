<?php

    return [
        'photo-master' => 'app/controllers/index.php',
        'photo-master/about' => 'app/controllers/about.php',
        'photo-master/asociados' => 'app/controllers/asociados.php',
        'photo-master/blog' => 'app/controllers/blog.php',
        'photo-master/contact' => 'app/controllers/contact.php',
        'photo-master/galeria' => 'app/controllers/galeria.php',
        'photo-master/post' => 'app/controllers/single_post.php'
    ];
?>
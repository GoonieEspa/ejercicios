<?php
require_once __DIR__.'/../database/QueryBuilder.php';

class AsociadoRepository extends QueryBuilder
{
    /**
     * AsociadoRepository constructor.
     * @param string $table
     * @param string $classEntity
     */
    public function __construct(string $table='Asociados',string $classEntity='Asociado')
    {
        parent::__construct($table,$classEntity);
    }
}
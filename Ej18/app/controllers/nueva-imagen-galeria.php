<?php
require_once 'utils/utils.php';
require_once 'exception/FileException.php';
require_once 'exception/AppException.php';
require_once 'exception/QueryException.php';
require_once 'utils/File.php';
require_once 'entity/ImageGallery.php';
require_once 'entity/Categoria.php';
require_once 'repository/ImagenGaleriaRepository.php';
require_once 'repository/CategoriaRepository.php';
require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';
require_once 'core/App.php';


$imagenes=[];

try{
    $descripcion=trim(htmlspecialchars($_POST['descripcion']));
    $categoria=trim(htmlspecialchars($_POST['categoria']));
    if(empty($categoria)){
        throw new ValidationException('No se ha recibido la categoría');
    }
    $tiposAceptados=['image/jpeg','image/png','image/gif'];
    $imagen=new File('imagen',$tiposAceptados);
    $imagen->saveUploadFile(ImageGallery::RUTA_IMAGENES_GALLERY);
    $imagen->copyFile(ImageGallery::RUTA_IMAGENES_GALLERY,ImageGallery::RUTA_IMAGENES_PORTFOLIO);
    $imagenGaleria = new ImageGallery($imagen->getFileName(),$descripcion,$categoria);
      
    $imgRepository=new ImagenGaleriaRepository();
    $imgRepository->guarda($imagenGaleria);
}
catch(FileException $fileException){

    die($fileException->getMessage());
}
catch(QueryException $queryException){
    die($queryException->getMessage());
}

catch(ValidationException $validationException){
    die($validationException->getMessage());
}

App::get('router')->redirect('photo-master/imagenes-galeria');
//header('location: /photo-master/imagenes-galeria');


?>
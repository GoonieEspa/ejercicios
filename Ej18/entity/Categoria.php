<?php
require_once __DIR__.'/../database/IEntity.php';

class Categoria implements IEntity
{
    private $id;
    private $nombre;
    private $numImagenes;


    /**
     * Categoria constructor.
     * @param $nombre
     * @param $numImagenes
     */
    public function __construct(string $nombre='', int $numImagenes=0)
    {
        $this->id=null;
        $this->nombre = $nombre;
        $this->numImagenes = $numImagenes;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     * @return Categoria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumImagenes()
    {
        return $this->numImagenes;
    }

    /**
     * @param mixed $numImagenes
     * @return Categoria
     */
    public function setNumImagenes($numImagenes)
    {
        $this->numImagenes = $numImagenes;
        return $this;
    }

    public function toArray(): array
    {
        return[
            'id'=>$this->getId(),
            'nombre'=>$this->getNombre(),
            'numImagenes'=>$this->getNumImagenes()
        ];
    }
}
<?php
require_once __DIR__.'/../database/IEntity.php';
class ImageGallery implements IEntity
{
    const RUTA_IMAGENES_PORTFOLIO='images/index/portfolio/';
    const RUTA_IMAGENES_GALLERY='images/index/gallery/';
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $nombre;
    /**
     * @var string
     */
    private $descripcion;
    /**
     * @var int
     */
    private $numVisualizaciones;
    /**
     * @var int
     */
    private $numLikes;
    /**
     * @var int
     */
    private $numDownloads;
    /**
     * @var int
     */
    private $categoria;

    /**
     * ImageGallery constructor.
     * @param string $nombre
     * @param string $descripcion
     * @param int $numVisualizaciones
     * @param int $numLikes
     * @param int $numDownloads
     */
    public function __construct(string $nombre="", string $descripcion="",int $categoria=0 ,int $numVisualizaciones=0, int $numLikes=0, int $numDownloads=0)
    {
        $this->id=null;
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->numVisualizaciones = $numVisualizaciones;
        $this->numLikes = $numLikes;
        $this->numDownloads = $numDownloads;
        $this->categoria=$categoria;
    }

    /**
     * @return int
     */
    public function getCategoria(): int
    {
        return $this->categoria;
    }

    /**
     * @param int $categoria
     * @return ImageGallery
     */
    public function setCategoria(int $categoria): ImageGallery
    {
        $this->categoria = $categoria;
        return $this;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getDescripcion();
    }

    public function getId()
    {
        return $this->id;
    }
    /**
     * @return string
     */
    public function getNombre() : string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return ImageGallery
     */
    public function setNombre(string $nombre) : ImageGallery
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion() : string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return ImageGallery
     */
    public function setDescripcion(string $descripcion) : ImageGallery
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumVisualizaciones() : int
    {
        return $this->numVisualizaciones;
    }

    /**
     * @param int $numVisualizaciones
     * @return ImageGallery
     */
    public function setNumVisualizaciones(int $numVisualizaciones) : ImageGallery
    {
        $this->numVisualizaciones = $numVisualizaciones;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumLikes() : int
    {
        return $this->numLikes;
    }

    /**
     * @param int $numLikes
     * @return ImageGallery
     */
    public function setNumLikes(int $numLikes) : ImageGallery
    {
        $this->numLikes = $numLikes;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumDownloads() : int
    {
        return $this->numDownloads;
    }

    /**
     * @param int $numDownloads
     * @return ImageGallery
     */
    public function setNumDownloads(int $numDownloads) : ImageGallery
    {
        $this->numDownloads = $numDownloads;
        return $this;
    }

    public function getUrlPortfolio() : string
    {
        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();
    }

    public function getUrlGallery() : string
    {
        return self::RUTA_IMAGENES_GALLERY . $this->getNombre();
    }

    public function toArray(): array
    {
       return[
           'id'=> $this->getId(),
           'nombre'=>$this->getNombre(),
           'descripcion'=>$this->getDescripcion(),
           'numVisualizaciones'=>$this->getNumVisualizaciones(),
           'numLikes'=>$this->getNumLikes(),
           'numDownloads'=>$this->getNumDownloads(),
           'categoria'=>$this->getCategoria()

       ];
    }
}


?>
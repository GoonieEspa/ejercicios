<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 8/01/19
 * Time: 20:03
 */

namespace App\Service;


class BDPruebaLibros
{

    private $arrayEnvol = array(
            array("isbn" => "A13412",
                "titulo" => "El niño del pijama de rayas",
                "autor" => "Alguien Famoso",
                "paginas" => 320),

            array("isbn" => "A441B3",
                "titulo" => "Pompeya",
                "autor" => "Juan Balderrama",
                "paginas" => 340),

            array("isbn" => "A64BDS",
                "titulo" => "Manifiesto Comunista",
                "autor" => "Karl Marx",
                "paginas" => 350)
    );

    public function get()
    {
        $result = $this->arrayEnvol;
        return $result;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 8/01/19
 * Time: 17:26
 */
namespace App\Controller;

use App\Entity\Editorial;
use App\Entity\Libro;

use App\Repository\LibroRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;


class LibroController extends AbstractController
{

    private $libros;

    public function __construct($bdPrueba)
    {

        $this->libros = $bdPrueba;
    }

    public function ficha($isbn){

        $libro=$this->getDoctrine()->getRepository(Libro::class);
        $result = $libro->find($isbn);
        if($result){
            return $this->render('ficha_libro.html.twig', Array('resp' => $result));
        }else{
            return $this->render('ficha_libro.html.twig', Array('resp' => null));
        }
    }

    public function insertar(){
        $entityManager=$this->getDoctrine()->getManager();

        $libro = new Libro();
        $libro->setIsbn("1111AAAA");
        $libro->setTitulo("Libro de prueba");
        $libro->setAutor("Autor de prueba");
        $libro->setPaginas(100);

        $entityManager->persist($libro);

        try {
            $entityManager->flush();
            return new Response("Libro insertado");
        }catch(\Exception $e){
            return new Response("Libro ya introducido");
        }

    }

    public function filtrarPaginas($paginas)
    {
        $entityManager=$this->getDoctrine()->getRepository(Libro::class);
        $cons = $entityManager->findByNumPaginas($paginas);

        return $this->render('inicio.html.twig', Array('libros' => $cons));

    }

    public function insertarConEditorial()
    {


        $entityManager=$this->getDoctrine()->getManager();
        $RepManager=$this->getDoctrine()->getRepository(Editorial::class);

        if($RepManager->findOneBy(['nombre' => "Santillana"])){
            $editorial = $RepManager->findOneBy(['nombre' => "Santillana"]);
        }else {
            $editorial = new Editorial();
            $editorial->setNombre("Santillana");
        }

        $libro = new Libro();
        $libro->setIsbn("4444DDDD");
        $libro->setTitulo("Libro de prueba con editorial2");
        $libro->setAutor("Autor de prueba con editorial2");
        $libro->setPaginas(1502);
        $libro->setEditorial($editorial);

        $entityManager->persist($editorial);
        $entityManager->persist($libro);
        try {
            $entityManager->flush();
            return new Response("Libro insertado");
        }catch(\Exception $e){
            return new Response("Libro ya introducido");
        }
    }


}

<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 8/01/19
 * Time: 17:09
 */
namespace App\Controller;

use App\Entity\Libro;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class InicioController extends AbstractController
{
    private $libros;

    public function __construct($bdPrueba)
    {
        $this->libros = $bdPrueba;
    }

    public function inicio(){

        $libro=$this->getDoctrine()->getRepository(Libro::class);
        $result = $libro->findAll();
        if($result){
            return $this->render('inicio.html.twig', Array('libros' => $result));
        }

    }

}
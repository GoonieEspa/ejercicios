<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LibroController extends AbstractController
{
    private $libros = array(
        array("isbn" => "9789655171990", "titulo" => "Lord of the rings",
            "autor" => "J. R. R. Tolkien", "paginas" => 1595),
        array("isbn" => "9783646927405", "titulo" => "The Maze Runner",
            "autor" => "James Dashner", "paginas" => 396),
        array("isbn" => "9781417685813", "titulo" => "Mortal Engine",
            "autor" => "Philip Reeve", "paginas" => 530),
        array("isbn" => "9780007491513", "titulo" => "I, robot",
            "autor" => "Isaac Asimov", "paginas" => 374),
        array("isbn" => "9780450054785", "titulo" => "The Shining",
            "autor" => "Stephen King", "paginas" => 447),
    );
    /**
     * @Route("/libro/{isbn}", name="ficha_libro", requirements={"isbn"="\d+"})
     */
    public function ficha($isbn)
    {
        $resultado = array_filter($this->libros, function($libro) use ($isbn)
            {
                return $libro["isbn"] == $isbn;
            });
        if (count($resultado) > 0)
        {
            return $this->render('ficha_libro.html.twig', array(
                'libro' => array_shift($resultado)
            ));
        }
        else
            return $this->render('ficha_libro.html.twig', array(
            'libro' => NULL
        ));
    }

    /**
     * @Route("/libro/{texto}", name="buscar_libro")
     */
    public function buscar($texto)
    {
        $resultado = array_filter($this->libros, 
        function($libro) use ($texto)
            {
                return strpos($libro["titulo"], $texto) !== FALSE;
            });
        return $this->render('lista_libros.html.twig', array(
            'libros' => $resultado
        ));
    }

}
?>